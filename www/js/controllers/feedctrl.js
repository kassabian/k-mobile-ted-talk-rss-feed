angular.module('tedrssapp.controllers.feedctrl', [])

.controller('FeedCtrl', function ($scope, FeedService, $ionicLoading) {
    console.log("Loading FeedCtrl");

    $ionicLoading.show({template: 'Loading feed...'});
    $scope.feed = FeedService;
    $scope.feed.loadFeed().then(function() {
      $ionicLoading.hide();

    });

    $scope.doRefresh = function () {
        $scope.feed.loadFeed().then(function () {
          $scope.$broadcast('scroll.refreshComplete');
        });
    };
});
