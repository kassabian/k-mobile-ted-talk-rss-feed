angular.module('tedrssapp', [
	'ionic',
	'ngCordova',
	'tedrssapp.controllers',
	'tedrssapp.services',
	'tedrssapp.filters'
])


.run(function ($ionicPlatform) {
	$ionicPlatform.ready(function () {
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
		}
		if (window.StatusBar) {
			StatusBar.styleDefault();
		}
	});
})

.constant("FEED_URL", "https://feeds.feedburner.com/TEDTalks_video")

.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('feed', {
        url: '/',
        templateUrl: 'templates/app/feed.html',
        controller: 'FeedCtrl'
      })
      .state('post', {
        url: '/post/:id',
        templateUrl: 'templates/app/post.html',
        controller: 'PostCtrl'
      });

	  $urlRouterProvider.otherwise('/');

});



